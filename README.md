[![Build Status](https://semaphoreci.com/api/v1/ricard/railingeditor/branches/master/shields_badge.svg)](https://semaphoreci.com/ricard/railingeditor)

# RailingEditor

This proof-of-concept was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.8 for learning purpose.

It handles an standard for designing rails with a codification standard.
Load figures code from `assets/figuras.json` into a editor component.
You will be able to edit the figure code, see validations and result in real time.

## Live
### You can see the prototype [HERE](https://ricard.github.io/railingEditor/)

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
