import { Component } from '@angular/core';
import { FiguresProviderService } from './figures-provider.service';
import { MatSnackBar } from '@angular/material';
// import { Observer } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  figuresCatalogue: Array<any>;
  
  constructor(private figures: FiguresProviderService, public snackBar: MatSnackBar) {
    this.init();
  }

  /**
   * Populates an array of figures and notify the number of
   * figures loaded by a snackbar component
   *
   * @memberof AppComponent
   */
  init() {
    this.figures.getAll().subscribe(result => {
      this.figuresCatalogue = result['formas'];
      this.snackBar.open('Loaded ' + this.figuresCatalogue.length + ' figures from file figures.json', '', {
        duration: 2000,
      });
    });
  }

  /**
   * Adds a new figure editor displacing the view to the top
   * Most of the code is for displacement, finally unshifts the figuresCatalogue array
   *
   * @memberof AppComponent
   */
  add() {
    let displacement = 5;
    let factor = 1.5;
    let scrollToTop = window.setInterval(() => {
      let pos = window.pageYOffset;
      if (pos > 0) {
        window.scrollTo(0, pos - displacement);
        displacement = displacement * factor;
      } else {
        window.clearInterval(scrollToTop);
        this.figuresCatalogue.unshift('AQW#enter here valid instructions @(angle) + L(longitude)#');
      }
    }, 20);
  }
}
