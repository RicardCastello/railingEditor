import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormControl, NgModel } from '@angular/forms';

@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.css']
})
export class EditorComponent implements OnInit {

  @ViewChild('codeInput') codeInput: NgModel;
  @Input() code: string;
  startingPoint = [100, 300]; // initial point where to start drawing
  path = [this.startingPoint]; // represents the scalar coordinates of each point to be draw
  validationErrors: Array<string>;
  accumulatedAngle: number = 0; // used as a reference to calculate next curvature
  accumulatedLongitude: number = 0;
  MAX_ANGLE = 165;
  MAX_LONGITUDE = 12000;

  ngOnInit() {
    this.generatePathFromCode();
  }

  /**
   * Starting from a code (AQW#@xLy...#)
   * populates the editor path as an array of pairs of scalar coordinates [[x,y],[x',y'], ...]
   *
   * @memberof EditorComponent
   */
  generatePathFromCode() {
    this.path = [this.startingPoint];
    this.validationErrors = [];
    this.accumulatedAngle = 0;
    this.accumulatedLongitude = 0;

    // previous validation of code
    this.syntacticValidation();

    // get the instructions (angle and longitude) as array
    this.parseInstructions()
      // loop instruccions until validation error
      .some(instruction => {
        const [angle, longitude] = instruction.split('L');
        if (isNaN(+longitude) || isNaN(+angle)) {
          this.recordError('Step not parseable to number "@' + angle + 'L' + longitude + '"');
          // the coordenate is not considered due a numeric parsing error
          return true;
        } else {
          this.accumulatedAngle += +angle;
          this.accumulatedLongitude += +longitude;
          this.path.push(this.polarToScalar(+longitude));
          return this.geometryValidation(+angle);
        }
      });

    this.codeInput.control.markAsTouched();
  }

  /**
   * Apply geometrical restrictions
   * - total longitude shoud be < MAX_LONGITUDE
   * - every angle must be -MAX_ANGLE < angle < MAX_ANGLE
   *
   * In case of incurring in error, calls recordError to store it and return true 
   * 
   * @param {*} angle
   * @returns {boolean}
   * @memberof EditorComponent
   */
  geometryValidation(angle): boolean {
    let errorFound = false;
    if (-this.MAX_ANGLE > angle || angle > this.MAX_ANGLE) {
      this.recordError('Angle restriction: -165º < angle < 165º');
      errorFound = true;
    }
    if (this.accumulatedLongitude > this.MAX_LONGITUDE) {
      this.recordError('Longitude restriction: 0 < longitude < 12,000');
      errorFound = true;
    }
    return errorFound;
  }

  /**
   * Syntactic validations of code:
   * - Starts with AQW#
   * - Ends with #
   * 
   * @memberof EditorComponent
   */
  syntacticValidation() {
    if (!new RegExp('^AQW#').test(this.code)) {
      this.recordError('Beginning with AQW#');
    }

    if (!new RegExp('#$').test(this.code)) {
      this.recordError('Ending with AQW#');
    }
  }

  /**
   * Register unique error messages on validationErrors array
   *
   * @param {*} msg
   * @memberof EditorComponent
   */
  recordError(msg: string) {
    if (this.validationErrors.indexOf(msg) === -1) {
      this.validationErrors.push(msg);
    }
    if (this.validationErrors.length === 1) {
      setTimeout(() => {
        this.codeInput.control.setErrors({ 'code validation': true })
      });
      this.codeInput.control.markAsTouched();
    }
  }

  /**
   * Parses the code string returning an array of instructions (@xLy)
   *
   * @returns {Array<string>}
   * @memberof EditorComponent
   */
  parseInstructions(): Array<string>{
    try {
      return this.code
        .split('AQW#')[1] // get the string after 'AQW#'
        .split('#')[0] // get the string before '#'
        .split('@') // split each instruction (@xLy)
        .slice(1); // remove first element because it is empty string
    } catch(e) {
      return [];
    }
  }

  
  /**
   * Convert polar coordinate to cartesian
   * given a angle and logintude, returns a X and Y coordinates that path
   * to apply the translation from the last X,Y coordinates (last line point)
   *
   * @param {*} longitude
   * @returns {[Number, Number]}
   * @memberof EditorComponent
   */
  polarToScalar(longitude) {
    const [lastX, lastY] = this.path[this.path.length - 1];
    const angle = this.accumulatedAngle * (Math.PI / 180);
    return [
      lastX + (longitude * Math.cos(angle)),
      lastY - (longitude * Math.sin(angle))
    ]
  }

}
