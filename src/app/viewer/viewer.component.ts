import { Component, OnChanges, Input, ViewChild } from '@angular/core';
import * as roughjs from 'roughjs';

@Component({
  selector: 'app-viewer',
  templateUrl: './viewer.component.html',
  styleUrls: ['./viewer.component.css']
})
export class ViewerComponent {

  @Input() path: Array<[number,number]>;
  @ViewChild('canvas') canvas;
  SCALE_FACTOR = 0.5;
  POINTS_DIAMETER = 10;
  circleOptions = {
    roughness: 0,
    fill: 'white',
    fillStyle: 'solid',
    stroke: '#15395d',
    strokeWidth: 5
  };
  lineOptions = {
    roughness: 0,
    stroke: '#15395d',
    strokeWidth: 8
  };

  ngOnChanges(changes) {
    if (changes['path']) {
      this.drawPath();
    }
  }

  drawPath() {
    // TODO: if any of the coordenates > canvas size --> canvas.scale(xFactor, yFactor)

    if (this.path.length > 1) {
      // const canvas = <HTMLCanvasElement>document.getElementById('canvas');
      const ctx = this.canvas.nativeElement.getContext('2d');
      const rc = roughjs.canvas(this.canvas.nativeElement);

      // reset scale
      ctx.setTransform(1, 0, 0, 1, 0, 0);
      // clear previous drawing
      ctx.clearRect(
        0, 0,
        // canvas.width * 1 / this.SCALE_FACTOR,
        // canvas.height * 1 / this.SCALE_FACTOR
        this.canvas.nativeElement.width,
        this.canvas.nativeElement.height
      );
      // apply the scale
      ctx.scale(this.SCALE_FACTOR, this.SCALE_FACTOR);

      // draw lines
      rc.linearPath(this.path, this.lineOptions);
      // draw points on lines intersections
      this.path.forEach(([x, y]) => {
        rc.circle(x, y, this.POINTS_DIAMETER, this.circleOptions);
      });
    }
  }

}
