import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import {
  MatToolbarModule,
  MatCardModule,
  MatFormFieldModule,
  MatInputModule,
  MatIconModule,
  MatButtonModule,
  MatSnackBarModule
} from '@angular/material';

import { AppComponent } from './app.component';
import { ViewerComponent } from './viewer/viewer.component';

import { FiguresProviderService } from './figures-provider.service';
import { EditorComponent } from './editor/editor.component';
import { CodePrettyfierPipe } from './code-prettyfier.pipe';

@NgModule({
  declarations: [
    AppComponent,
    ViewerComponent,
    EditorComponent,
    CodePrettyfierPipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    MatToolbarModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    MatSnackBarModule,
    BrowserAnimationsModule,
    FormsModule
  ],
  providers: [
    FiguresProviderService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
