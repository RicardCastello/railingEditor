import { TestBed, inject } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { FiguresProviderService } from './figures-provider.service';

describe('FiguresProviderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [FiguresProviderService]
    });
  });

  it('should be created', inject([FiguresProviderService], (service: FiguresProviderService) => {
    expect(service).toBeTruthy();
  }));
});
