import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'codePrettyfier'
})
export class CodePrettyfierPipe implements PipeTransform {

  PREFIX = new RegExp('^AQW#');
  SUFFIX = new RegExp('#$');

  /**
   * Removes 'AQW#' prefix and '#' suffix
   *
   * @param {*} value
   * @returns {*}
   * @memberof CodePrettyfierPipe
   */
  transform(value: any): any {
    return value
      .replace(this.PREFIX, '')
      .replace(this.SUFFIX,'');
  }

}